package zoop.roadcrossinggame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

class Block {

    private Rect rect;
    private float horizSpd;
    private Bitmap img;
    private int startY;
    private Const.BlockType type;

    Block(int X, int Y, int width, int height, float horizSpd, Const.BlockType type, Context context) {
        int startX = Const.BLOCK_WIDTH * X;
        this.startY = Const.BLOCK_HEIGHT * Y;
        this.horizSpd = horizSpd;
        this.rect = new Rect(startX, startY, startX+(Const.BLOCK_WIDTH*width), startY+(Const.BLOCK_HEIGHT*height));
        this.type = type;
        switch(type) {
            case CAR:
                int carSelect = (int) Math.ceil(Math.random() * 3);
                if(carSelect == 1) img = BitmapFactory.decodeResource(context.getResources(), R.drawable.car1);
                if(carSelect == 2) img = BitmapFactory.decodeResource(context.getResources(), R.drawable.car2);
                if(carSelect == 3) img = BitmapFactory.decodeResource(context.getResources(), R.drawable.car3);
                break;
            case WATER:
                img = BitmapFactory.decodeResource(context.getResources(), R.drawable.water);
                break;
            case FINISH:
                img = BitmapFactory.decodeResource(context.getResources(), R.drawable.finish);
                break;
            case GRASS:
                img = BitmapFactory.decodeResource(context.getResources(), R.drawable.grass);
                break;
            case ROAD:
                img = BitmapFactory.decodeResource(context.getResources(), R.drawable.road);
                break;
            case LOG:
                img = BitmapFactory.decodeResource(context.getResources(), R.drawable.log);
                break;
            case TRUCK:
                //img = BitmapFactory.decodeResource(context.getResources(), R.drawable.truck);
                break;
            default:
                img = BitmapFactory.decodeResource(context.getResources(), R.drawable.road);
                break;
        }
    }

    void init() {
        rect.top = startY;
        rect.bottom = startY + Const.BLOCK_HEIGHT;
    }

    float getHorizSpd() { return horizSpd; }

    Rect getRect() { return rect; }

    Const.BlockType getType() { return type; }

    void draw(Canvas canvas) {
        canvas.drawBitmap(img, null, rect, new Paint());
    }

    void update() {
        rect.left += horizSpd;
        rect.right += horizSpd;
        if(horizSpd != 0) {
            if(rect.left > Const.SCREEN_WIDTH && horizSpd > 0) {
                rect.left = -Const.BLOCK_WIDTH;
                rect.right = 0;
            }
            if(rect.right < 0 && horizSpd < 0) {
                rect.left = Const.SCREEN_WIDTH;
                rect.right = Const.SCREEN_WIDTH + Const.BLOCK_WIDTH;
            }
        }

    }
}
