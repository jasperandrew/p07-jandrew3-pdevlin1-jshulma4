package zoop.roadcrossinggame;

import android.content.Context;
import android.graphics.Canvas;

import java.util.ArrayList;

class BlockManager {
    private ArrayList<Block> blocks;

    BlockManager(Context context) {
        blocks = new ArrayList<>();
        //Create landscape
        for(int i = 0; i <= Const.NUM_ROWS; i++) {
            for(int j = 0; j <= Const.NUM_COLUMNS; j++) {
                int k = (i)% 6;
                if(i == 0) {
                    blocks.add(new Block(j,i,1,1,0, Const.BlockType.FINISH, context));
                }
                else if((i < Const.NUM_ROWS - 1 && k == 1) || (i < Const.NUM_ROWS && k == 2)) {
                    blocks.add(new Block(j,i,1,1,0, Const.BlockType.WATER, context));
                    if(k == 1 && j == Const.NUM_COLUMNS) {
                        blocks.add(new Block(j,i,1,1,
                                Const.LEFT*Const.LOG_SPEED, Const.BlockType.LOG, context));
                        blocks.add(new Block(j+(Const.NUM_COLUMNS/2),i,1,1,
                                Const.LEFT*Const.LOG_SPEED, Const.BlockType.LOG, context));
                    }
                    if(k == 2 && j == Const.NUM_COLUMNS) {
                        blocks.add(new Block(0,i,1,1,
                                Const.RIGHT*Const.LOG_SPEED, Const.BlockType.LOG, context));
                        blocks.add(new Block(-(2*Const.NUM_COLUMNS/3),i,1,1,
                                Const.RIGHT*Const.LOG_SPEED, Const.BlockType.LOG, context));
                    }
                }
                else if((i < Const.NUM_ROWS - 1 && k == 4) || (i < Const.NUM_ROWS && k == 5)) {
                    blocks.add(new Block(j, i, 1, 1, 0, Const.BlockType.ROAD, context));
                    if (k == 4 && j == Const.NUM_COLUMNS) {
                        blocks.add(new Block(j,i,1,1,
                                Const.LEFT*Const.CAR_SPEED, Const.BlockType.CAR, context));
                        blocks.add(new Block(j+(Const.NUM_COLUMNS/2),i,1,1,
                                Const.LEFT*Const.CAR_SPEED, Const.BlockType.CAR, context));
                    }
                    if (k == 5 && j == Const.NUM_COLUMNS) {
                        blocks.add(new Block(0,i,1,1,
                                Const.RIGHT*Const.CAR_SPEED, Const.BlockType.CAR, context));
                        blocks.add(new Block(-(2*Const.NUM_COLUMNS/3),i,1,1,
                                Const.RIGHT*Const.CAR_SPEED, Const.BlockType.CAR, context));
                    }
                }
                else {
                    blocks.add(new Block(j,i,1,1,0, Const.BlockType.GRASS, context));
                }
            }
        }
    }

    void init() {
        //this will create the layout of the blocks (fill the array list)
    }

    ArrayList<Block> getBlocks() { return blocks; }

    void draw(Canvas canvas) {
        for(Block block : blocks){
            block.draw(canvas);
        }
    }

    void update() {
        for(Block block : blocks){
            block.update();
        }
    }
}
