package zoop.roadcrossinggame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.SystemClock;

/**
 * Created by Peter on 4/20/2017.
 */

public class GameOverScreen {
    private Paint textPaint;
    boolean winner;
    long endTime;
    double totalTime;

    GameOverScreen(boolean lost) {
        textPaint = new Paint();
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(150);
        textPaint.setFakeBoldText(true);
        textPaint.setColor(Color.WHITE);
        winner = !lost;
        totalTime = (Const.END_TIME-Const.START_TIME)/1000.0;
    }

    void draw(Canvas canvas) {

        if(winner)
        {
            canvas.drawText("YOU", Const.SCREEN_WIDTH / 2, Const.SCREEN_HEIGHT / 2 - 200, textPaint);
            canvas.drawText("WIN", Const.SCREEN_WIDTH / 2, Const.SCREEN_HEIGHT / 2 + 100, textPaint);
            textPaint.setTextSize(90);
            canvas.drawText("It took you " + totalTime + " seconds", Const.SCREEN_WIDTH / 2, Const.SCREEN_HEIGHT / 2 + 200, textPaint);
        }
        else {
            canvas.drawText("GAME", Const.SCREEN_WIDTH / 2, Const.SCREEN_HEIGHT / 2 - 200, textPaint);
            canvas.drawText("OVER", Const.SCREEN_WIDTH / 2, Const.SCREEN_HEIGHT / 2 + 100, textPaint);
        }
        textPaint.setTextSize(90);
        canvas.drawText("Swipe down to play again", Const.SCREEN_WIDTH/2, Const.SCREEN_HEIGHT/2+300, textPaint);
    }
}

