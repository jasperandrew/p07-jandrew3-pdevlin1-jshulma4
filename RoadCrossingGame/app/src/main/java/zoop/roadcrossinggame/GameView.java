package zoop.roadcrossinggame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private float x1, x2, y1, y2, dx, dy;
    private Const.Direction direction;
    public static BlockManager blockManager;
    public static Jumper jumper;
    public static long startTime;

    public GameView(Context context) {
        super(context);
        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        blockManager = new BlockManager(context);
        jumper = new Jumper(Const.NUM_COLUMNS/2, Const.NUM_ROWS, Const.BLOCK_WIDTH, Const.BLOCK_HEIGHT, context);

        Const.START_TIME = SystemClock.elapsedRealtime();

        setFocusable(true);
    }

    public void update() {
        if(!Const.GAME_OVER) {
            blockManager.update();
            int gameOver = jumper.update();
            if(gameOver == 1)
            {
                Const.GAME_OVER = true;
                Const.GAME_LOST = true;
            }
            else if (gameOver == 2)
            {
                Const.GAME_OVER = true;
                Const.GAME_LOST = false;
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(!Const.GAME_OVER){
            canvas.drawColor(Color.BLACK);
            blockManager.draw(canvas);
            jumper.draw(canvas);
        }else{
            if(Const.FIRST_OVER)
            {
                Const.END_TIME = SystemClock.elapsedRealtime();
                Const.FIRST_OVER = false;
            }
            GameOverScreen gameOverScreen = new GameOverScreen(Const.GAME_LOST);
            gameOverScreen.draw(canvas);
        }
    }

    private void resetGame() {
        Const.GAME_OVER = false;
        Const.START_TIME = SystemClock.elapsedRealtime();
        Const.FIRST_OVER = true;
        blockManager.init();
        jumper.init();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()){
            case(MotionEvent.ACTION_DOWN):
                if(Const.GAME_OVER){
                    resetGame();
                    return true;
                }
                x1 = event.getX();
                y1 = event.getY();
                break;

            case(MotionEvent.ACTION_UP):
                x2 = event.getX();
                y2 = event.getY();
                dx = x2-x1;
                dy = y2-y1;

                // Use dx and dy to determine the direction
                if(Math.abs(dx) > Math.abs(dy)) {
                    if(dx > 0)
                        direction = Const.Direction.RIGHT;
                    else
                        direction = Const.Direction.LEFT;
                } else {
                    if(dy > 0)
                        direction = Const.Direction.DOWN;
                    else
                        direction = Const.Direction.UP;
                }
                jumper.setDirection(direction);
                break;
            default:
                jumper.setDirection(Const.Direction.NONE);
        }
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while(retry){
            try {
                thread.setRunning(false);
                thread.join();
            } catch(Exception e) { e.printStackTrace(); }

            retry = false;
        }
    }

}