package zoop.roadcrossinggame;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;

public class Jumper{

    private Rect rect;
    private Bitmap img;
    private Const.Direction direction;
    private int startY, startX;
    MediaPlayer mp;
    Context cont;

    Jumper(int X, int Y, int widthIn, int heightIn, Context context) {
        startX = Const.BLOCK_WIDTH * X;
        startY = Const.BLOCK_HEIGHT * Y;
        direction = Const.Direction.NONE;
        rect = new Rect(startX, startY, startX+widthIn, startY+heightIn);
        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.frog);
        cont = context;
    }

    void init() {
        rect.top = startY;
        rect.bottom = startY + Const.BLOCK_HEIGHT;
        rect.left = startX;
        rect.right = startX + Const.BLOCK_WIDTH;
    }

    void setDirection(Const.Direction dir) {
        direction = dir;
    }

    void draw(Canvas canvas) {
        canvas.drawBitmap(img, null, rect, new Paint());
    }

    int update() {
        int gameOver = 0;
        boolean on_log = false, dead = false;
        float horizSpd = 0;
        for(Block block : GameView.blockManager.getBlocks()){
            if(block.getType() == Const.BlockType.LOG && block.getRect().contains(rect.centerX(), rect.centerY())){
                on_log = true;
                gameOver = 0;
                horizSpd = block.getHorizSpd();
            }
            else if(block.getType() == Const.BlockType.WATER && block.getRect().contains(rect.centerX(), rect.centerY())) {
                dead = true;
                gameOver = 1;
                //if(!on_log) playSound(R.raw.splash);
            }
            else if (block.getType() == Const.BlockType.CAR && block.getRect().contains(rect.centerX(), rect.centerY())){
                dead = true;
                gameOver = 1;
                playSound(R.raw.splat);
            }
            else if(block.getType() == Const.BlockType.FINISH && block.getRect().contains(rect.centerX(), rect.centerY())){
                gameOver = 2;
                playSound(R.raw.yay);
            }
        }

        if(on_log){
            rect.left += horizSpd;
            rect.right += horizSpd;
        }else{
            if(dead) init();
        }

        switch(direction) {
            case UP:
                if(rect.top > 0) {
                    rect.top -= Const.BLOCK_HEIGHT;
                    rect.bottom -= Const.BLOCK_HEIGHT;
                }
                break;
            case DOWN:
                if(rect.bottom < Const.SCREEN_HEIGHT) {
                    rect.top += Const.BLOCK_HEIGHT;
                    rect.bottom += Const.BLOCK_HEIGHT;
                }
                break;
            case LEFT:
                if(rect.left > 0) {
                    rect.left -= Const.BLOCK_WIDTH;
                    rect.right -= Const.BLOCK_WIDTH;
                }
                break;
            case RIGHT:
                if(rect.right < Const.SCREEN_WIDTH) {
                    rect.right += Const.BLOCK_WIDTH;
                    rect.left += Const.BLOCK_WIDTH;
                }
                break;
            default:
        }
        direction = Const.Direction.NONE;
        return gameOver;
    }

    public void playSound(int sound)
    {
        if(mp != null)
        {
            mp.stop();
            mp.release();
        }
        mp = MediaPlayer.create(cont, sound);
        if(mp != null)
        {
            mp.start();
        }
    }
}
