package zoop.roadcrossinggame;

class Const {
    static final int MAX_FPS = 60;
    static final int LOG_SPEED = 3;
    static final int CAR_SPEED = 6;
    static final int LEFT = -1;
    static final int RIGHT = 1;
    static int SCREEN_WIDTH;
    static int SCREEN_HEIGHT;
    static int BLOCK_HEIGHT;
    static int BLOCK_WIDTH;
    static int NUM_COLUMNS;
    static int NUM_ROWS;
    public enum BlockType {
        CAR, TRUCK, LOG, WATER, GRASS, ROAD, FINISH
    }
    public enum Direction {
        UP, DOWN, LEFT, RIGHT, NONE
    }
    static long START_TIME;
    static long END_TIME;
    static boolean FIRST_OVER = true;
    static boolean GAME_OVER = false;
    static boolean GAME_LOST = false;
}
